<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tugas Hana</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body>

    <div class="card shadow p-3 col-8 offset-2 mt-5">

    <h3>Tambah Data</h3>
    <form action="/cast" method="POST">
        @csrf
        <div class="mb-3">
            <label class="title-section-content" for="">Nama</label>
            <input value="{{old('nama')}}" name="nama" type="text"
                class="form-control" placeholder="">
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Umur</label>
            <input value="{{old('umur')}}" name="umur" type="text"
                class="form-control" placeholder="">
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Bio</label>
            <input value="{{old('bio')}}" name="bio" type="text"
                class="form-control" placeholder="">
        </div>
        <div class="mb-3 mt-4">
        <a href="/cast" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary" type="submit">Tambah Data</button>
    </div>
    </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
    </script>
</body>

</html>
